export default {
    openStory: (storyField) => {
        const url = "https://jira.rel.apps.telenet.be/browse/";
        const storyPrefix = 'BASE-';
        const story = storyField.value;
        const win = window.open(url + storyPrefix + story, '_blank');
        win.focus();
    }
}
