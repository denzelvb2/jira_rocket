import NotificationHandler from './notifications/NotificationHandler.js';
import Repository from './repository/Repository.js';
import DOMHandler from './handlers/DOMHandler.js';
import Subtask from './model/Subtask.js';
import LoadingHandler from './handlers/LoadingHandler.js';

const SubtaskGenerator = {

    init: () => {
        // initialise DOM
        DOMHandler.init();
    },
    submitSubtasks: () => {
        const story = document.getElementById('story-input').value;
        NotificationHandler.closeNotifications();

        const teamSelect = document.getElementById('team-select');
        const teamId = teamSelect.options[teamSelect.selectedIndex].value;
        const teamName = teamSelect.options[teamSelect.selectedIndex].text;
        const team = { name: teamName, id: teamId };

        const labelSelect = document.getElementById('label-select');
        const label = labelSelect.options[labelSelect.selectedIndex].value;

        const subtasks = SubtaskGenerator.getSubtasks();
        SubtaskGenerator.generateSubtasks(story, subtasks, team, label);
    },
    generateSubtasks: (story, subtasks, team, label) => {
        let count = 0;
        LoadingHandler.startLoading();

        for (let subtask of subtasks) {
            const summary = subtask.summary;
            const description = subtask.description;
            Repository.generateSubtask(story, summary, description, team, label).then(() => {
                count++;
                if (count === subtasks.length) {
                    NotificationHandler.setSuccess('Subtasks successfully generated!');
                    ga_trackGeneratedSubtasks(story, subtasks.length);
                    LoadingHandler.stopLoading();
                    DOMHandler.clearFields();
                    DOMHandler.disableButtons();
                }
            }).catch((err) => {
                LoadingHandler.stopLoading();
                NotificationHandler.setError(err, true);
            })
        }
    },
    getSubtasksAmnt: (amount) => {
        const techVal = document.getElementById('techval-checkbox');
        const funcVal = document.getElementById('funcval-checkbox');
        if (techVal.checked) amount++;
        if (funcVal.checked) amount++;
        return amount;
    },
    getSubtasks: () => {
        const subtasks = [];
        const subtaskFields = document.querySelectorAll('.subtask-fields .input-field');
        for (let subtaskField of subtaskFields) {
            const summary = subtaskField.querySelector('.subtask-summary').value.trim();
            const description = subtaskField.querySelector('.subtask-description').value.trim();
            if (summary !== '') {
                subtasks.push(new Subtask(summary, description));
            }
        }

        const techVal = document.getElementById('techval-checkbox');
        if (techVal.checked) {
            subtasks.push(new Subtask('[VAL] Technical validation', ''));
        }

        const funcVal = document.getElementById('funcval-checkbox');
        if (funcVal.checked) {
            subtasks.push(new Subtask('[VAL] Functional validation', ''));
        }

        return subtasks;
    }
}

export default SubtaskGenerator;