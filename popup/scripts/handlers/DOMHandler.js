import Repository from './../repository/Repository.js';
import NotificationHandler from './../notifications/NotificationHandler.js';
import EventListeners from './EventListeners.js';

const DOMHandler = {
    init: () => {
        EventListeners.init();
        // fill in team options
        DOMHandler.fillTeams();
        DOMHandler.disableButtons();
        DOMHandler.showVersion();
    },
    fillTeams: async () => {
        const teams = await Repository.fetchTeams()
            .then((result) => {
                return result;
            })
            .catch((err) => {
                NotificationHandler.setError(err);
            });
        DOMHandler.fillTeamsSelect(teams);
    },
    fillTeamsSelect: (teams) => {
        const select = document.getElementById('team-select');
        for (let team of teams) {
            const option = document.createElement('option');
            option.text = team.name;
            option.value = team.id;
            select.add(option);
        }
    },
    disableButtons: () => {
        document.querySelectorAll('.btn-disable').forEach(btn => btn.disabled = true);
    },
    enableButtons: () => {
        document.querySelectorAll('.btn-disable').forEach(btn => btn.disabled = false);
    },
    createSubtaskFields: () => {
        const subtasksContainer = document.getElementById('subtask-fields');

        const subtaskFields = document.createElement('div');
        subtaskFields.className = "input-field borderBottom"

        const summaryField = DOMHandler.createSummaryField();
        const descriptionField = DOMHandler.createDescriptionField();
        const deleteSpan = DOMHandler.createDeleteSpan(subtaskFields);

        subtaskFields.append(summaryField, descriptionField, deleteSpan);
        subtasksContainer.append(subtaskFields);
    },
    createSummaryField: () => {
        const summaryField = document.createElement('input');
        summaryField.type = "text";
        summaryField.placeholder = "Summary";
        summaryField.className = "subtask-summary";
        EventListeners.addPrefixListener(summaryField);
        return summaryField;
    },
    createDescriptionField: () => {
        const descriptionField = document.createElement('textarea');
        descriptionField.placeholder = "Description";
        descriptionField.className = "subtask-description";
        descriptionField.onkeydown = DOMHandler.handleKeyDown;
        return descriptionField;
    },

    createDeleteSpan: (elToRemove) => {
        const deleteSpan = document.createElement('span');
        deleteSpan.textContent = "X";
        deleteSpan.className = "subtask-delete";
        deleteSpan.onclick = function () { DOMHandler.removeElement(elToRemove); };
        return deleteSpan;
    },
    removeElement: (element) => {
        element.parentNode.removeChild(element);
    },
    handleKeyDown: (e) => {
        if (e.which == 9) {
            DOMHandler.removeEmptySubtaskFields();
            DOMHandler.createSubtaskFields();
        }
    },
    removeEmptySubtaskFields: () => {
        const subtaskFields = document.querySelectorAll('.subtask-fields .input-field');
        for (const [i, subtask] of subtaskFields.entries()) {
            const summary = subtask.querySelector('.subtask-summary');
            if (summary.value.trim() === '' && i !== 0) subtask.parentNode.removeChild(subtask);
        }
    },
    clearFields: () => {
        const subtaskFields = document.querySelectorAll('.subtask-fields .input-field');
        for (const entry of subtaskFields.entries()) {
            entry[1].querySelector('.subtask-summary').value = "";
            entry[1].querySelector('.subtask-description').value = "";
        }
        DOMHandler.removeEmptySubtaskFields();
        document.getElementById('story-input').value = "";
        document.getElementById('techval-checkbox').checked = false;
        document.getElementById('funcval-checkbox').checked = false;
    },
    hideAll: () => {
        document.getElementById('generateSubtasks').style.display = 'none';
    },
    showVersion: () => {
        document.getElementById('version').textContent = 'Version: ' + chrome.runtime.getManifest().version;
    }
}

export default DOMHandler;