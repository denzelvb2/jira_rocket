import SubtaskGenerator from './../SubtaskGenerator.js';
import Redirector from './../redirector/Redirector.js';
import DOMHandler from './DOMHandler.js';
import NotificationHandler from './../notifications/NotificationHandler.js';
const EventListeners = {
    init: () => {
        document.getElementById('submitSubtaks').onclick = SubtaskGenerator.submitSubtasks;
        document.querySelectorAll('.subtask-description')
            .forEach(el => el.onkeydown = DOMHandler.handleKeyDown);
        const storyInput = document.getElementById('story-input');
        storyInput.addEventListener('input', (e) => {
            e.target.value === "" ? DOMHandler.disableButtons() : DOMHandler.enableButtons();
        })
        document.getElementById('story-open-btn').onclick = function () { Redirector.openStory(storyInput) };
        document.querySelectorAll('.subtask-summary').forEach(element => {
            EventListeners.addPrefixListener(element);
        });
        document.getElementById('btn-copy').onclick = EventListeners.copyErrorToClipboard;
        window.onerror = EventListeners.handleWindowError;
    },
    addPrefixListener: (element) => {
        element.addEventListener('input', (evt) => {
            switch (evt.target.value) {
                case 'dev  ':
                    evt.target.value = '[DEV] ';
                    break;
                case 'test  ':
                    evt.target.value = '[TEST] ';
                    break;
                case 'val  ':
                    evt.target.value = '[VAL] ';
                    break;
                case 'doc  ':
                    evt.target.value = '[DOC] ';
                    break;
                case 'ana  ':
                    evt.target.value = '[ANA] ';
            }
        });
    },
    handleWindowError: (msg, url, lineNo, columnNo) => {
        NotificationHandler.setError(`Oops! Something wrong happened... \r\n 
            Please inform the developer of this error. \r\n
            [${msg} in file ${url.split('/').pop()} at line ${lineNo}:${columnNo}]`, true);
    },
    copyErrorToClipboard: () => {
        const errorMsg = document.getElementById('errorMessage').textContent;
        let filteredErrorMsg = errorMsg.substring(
            errorMsg.lastIndexOf('[') + 1,
            errorMsg.lastIndexOf(']')
        );

        const el = document.createElement('textarea');
        el.value = filteredErrorMsg.trim() !== '' ? filteredErrorMsg : errorMsg;
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        NotificationHandler.setClipboardSuccess();
    },
}

export default EventListeners;