export default {
    startLoading: () => {
        document.getElementById('loader').style.display = "inline-block";
        document.getElementById('generateSubtasks').style.display = "none";
    },
    stopLoading: () => {
        document.getElementById('loader').style.display = "none";
        document.getElementById('generateSubtasks').style.display = "inline-block";
    }
}