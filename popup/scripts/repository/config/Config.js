const Config = {
    url: "https://jira.rel.apps.telenet.be/rest/api/2/",
    headers: {
        "Content-Type": "application/json"
    }
}

export default Config;