import Config from './config/Config.js';

const Repository = {
	generateSubtask: (story, summary, description, team, label) => {
		const subtaskBody = {
			"fields":
			{
				"project":
				{
					"key": "BASE"
				},
				"labels": [
					label
				],
				"parent":
				{
					"key": `BASE-${story}`
				},
				"summary": summary,
				"description": description,
				"issuetype":
				{
					"id": "5"
				}
			}
		}

		return fetch(Config.url + 'issue/', {
			method: "POST",
			headers: Config.headers,
			body: JSON.stringify(subtaskBody)
		})
			.then((res) => {
				if (res.ok) {
					return res.json();
				} else {
					throw ('Error creating the subtask');
				}
			})
			.then((response) => {
				if (response !== null) {
					return Repository.updateTeam(response.key, team);
				}
			})
			.catch((error) => {
				throw ('Network error on creating the subtask');
			});
	},
	updateTeam: (key, team) => {
		const updateTeamBody = {
			"fields": {
				"customfield_14800": {
					"self": "https://jira.rel.apps.telenet.be/rest/api/2/customFieldOption/14200",
					"value": "BASE Tribe",
					"id": "14200",
					"child": {
						"self": "https://jira.rel.apps.telenet.be/rest/api/2/customFieldOption/" + team.id,
						"value": team.name,
						"id": team.id
					}
				}
			}
		}

		return fetch(Config.url + 'issue/' + key, {
			method: "PUT",
			headers: Config.headers,
			body: JSON.stringify(updateTeamBody)
		}).then((res) => {
			console.info(`[Updating team of story BASE-${key}...]`);
			if (res.ok) {
				return res;
			} else {
				throw (`Error updating the team in the story ${key}`);
			}
		}).catch((error) => {
			throw (`Network error in updating the team in the story ${key}`);
		})
	},
	fetchTeams: async () => {
		let teams = [];

		console.info(`[Fetching teams...]`);
		// Team ID's start at 14201, end at 14206.
		for (let i = 14201; i <= 14206; i++) {
			const teamId = i;
			const teamName = await fetch(Config.url + 'customFieldOption/' + i,
				{
					method: "GET",
					headers: Config.headers
				})
				.then((res) => {
					if (res.ok) {
						return res.json();
					} else {
						throw ('Error fetching the teams (Network or JIRA issues)');
					}
				})
				.then((response) => {
					return response.value;
				})
				.catch((error) => {
					throw ('Network error in fetching the teams');
				})
			const team = { name: teamName, id: teamId };
			teams.push(team);
		}
		return teams;
	}
}

export default Repository;