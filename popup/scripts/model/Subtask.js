class Subtask {
    constructor(summary, description) {
        this.summary = summary;
        this.description = description;
    }
}

export default Subtask;