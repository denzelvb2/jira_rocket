export default {
    setError: (msg, showCopy = false) => {
        const errorField = document.getElementById('errorMessage');
        errorField.style.display = 'block';
        errorField.textContent = msg;
        if(showCopy) {
            document.getElementById('btn-copy').style.display = 'block';
        }
    },
    closeNotifications: () => {
        const errorField = document.getElementById('errorMessage');
        errorField.style.display = 'none';
        errorField.textContent = '';
        const successField = document.getElementById('successMessage');
        successField.style.display = 'none';
        successField.textContent = '';
        document.getElementById('btn-copy').style.display = 'none';
        document.getElementById('copy-success').style.display = 'none';
    },
    setSuccess: (msg) => {
        const successField = document.getElementById('successMessage');
        successField.style.display = 'block';
        successField.textContent = msg;
    },
    setClipboardSuccess: () => {
        document.getElementById('copy-success').style.display = 'flex';
    }
}
