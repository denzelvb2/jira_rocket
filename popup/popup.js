import SubtaskGenerator from './scripts/SubtaskGenerator.js';
import NotificationHandler from './scripts/notifications/NotificationHandler.js';
import DOMHandler from './scripts/handlers/DOMHandler.js';
const init = () => {
    if (chrome.tabs) {
        chrome.tabs.query(
            {
                "status": "complete",
                "windowId": chrome.windows.WINDOW_ID_CURRENT,
                "active": true
            }, (tab) => {
                if (tab[0]) {
                    chrome.cookies.getAll(
                        { "url": tab[0].url, "name": "JSESSIONID" },
                        (cookie) => {
                            if (cookie.length) {
                                SubtaskGenerator.init();
                            } else {
                                NotificationHandler.setError('Cannot read cookie \'JSESSIONID\'. Please open JIRA!')
                            }
                        });
                } else {
                    NotificationHandler.setError('Error reading tab. Please wait for the page to be fully loaded and re-open extension.')
                    DOMHandler.hideAll();
                }

            });
    } else {
        NotificationHandler.setError('Extension is not opened within a tab.')
        DOMHandler.hideAll();
    }
}

window.onload = init;

